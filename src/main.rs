extern crate rand;

use rand::prelude::*;
use std::cmp;

const PRICE_BUY: f64 = 0.33;
const PRICE_SELL: f64 = 0.50;
const PRICE_RECYCLE: f64 = 0.05;
const NUM_DAYS: [usize; 3] = [1000, 10000, 100_000];
const NUM_BUY: [usize; 7] = [40, 50, 60, 70, 80, 90, 100];

const GOOD_DEMANDS: &[usize; 7] = &NUM_BUY;
const FAIR_DEMANDS: &[usize; 7] = &GOOD_DEMANDS;
const POOR_DEMANDS: &[usize; 7] = &GOOD_DEMANDS;

const GOOD_PROBABILITIES: [f64; 7] = [0.03, 0.05, 0.15, 0.20, 0.35, 0.15, 0.07];
const FAIR_PROBABILITIES: [f64; 7] = [0.10, 0.18, 0.40, 0.20, 0.08, 0.04, 0.00];
const POOR_PROBABILITIES: [f64; 7] = [0.44, 0.22, 0.16, 0.12, 0.06, 0.00, 0.00];

fn get_day_demand(rand_day: f64, probabilities: &[f64], demands: &[usize]) -> usize {
    let mut total_probability = 0.0;
    for (i, probability) in probabilities.iter().enumerate() {
        total_probability += probability;
        if rand_day < total_probability {
            return demands[i];
        }
    }
    0
}

fn get_buy_cost(nbuy: usize) -> f64 {
    nbuy as f64 * PRICE_BUY
}

fn get_sell_cost(amount_sold: usize) -> f64 {
    amount_sold as f64 * PRICE_SELL
}

fn get_revenue(ddemand: usize, nbuy: usize) -> f64 {
    let amount_sold = cmp::min(ddemand, nbuy);
    let buy_cost = get_buy_cost(nbuy);
    let sell_cost = get_sell_cost(amount_sold);
    ((sell_cost - buy_cost) * 100.0).round() / 100.0
}

fn get_lost_profit(ddemand: usize, nbuy: usize) -> f64 {
    (ddemand - nbuy) as f64 * (PRICE_SELL - PRICE_BUY)
}

fn get_recycle(ddemand: usize, nbuy: usize) -> f64 {
    (nbuy - ddemand) as f64 * PRICE_RECYCLE
}

fn get_day_profit(nbuy: usize, rand_daytype: f64, rand_day_demand: f64) -> f64 {
    let (probabilities, demands) = if rand_daytype < 0.36 {
        (GOOD_PROBABILITIES, GOOD_DEMANDS)
    } else if rand_daytype < 0.81 {
        (FAIR_PROBABILITIES, FAIR_DEMANDS)
    } else {
        (POOR_PROBABILITIES, POOR_DEMANDS)
    };
    let ddemand = get_day_demand(rand_day_demand, &probabilities, demands);

    let revenue = get_revenue(ddemand, nbuy);
    let (lost_profit, recycle) = if ddemand > nbuy {
        (get_lost_profit(ddemand, nbuy), 0.0)
    } else {
        (0.0, get_recycle(ddemand, nbuy))
    };
    revenue - lost_profit + recycle
}

fn main() {
    let seed: u64 = 0;
    let mut rng: StdRng = rand::SeedableRng::seed_from_u64(seed);

    for &days in NUM_DAYS.iter() {
        let mut optimal = 0.0;
        let mut optimal_buy = 0;
        for &buy in NUM_BUY.iter() {
            let sum_profit = (0..days)
                .map(|_| get_day_profit(buy, rng.gen(), rng.gen()))
                .sum();
            println!(
                "days: {days} \t{buy} newspapers purchased \tprofit: ${profit:.2}",
                days = days,
                buy = buy,
                profit = sum_profit
            );
            if sum_profit > optimal {
                optimal_buy = buy;
                optimal = sum_profit;
            }
        }
        println!("optimal for {} newspapers: ${:.2}\n", optimal_buy, optimal)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day_profit() {
        assert_eq!(get_revenue(60, 70), 6.90);
        assert_eq!(get_buy_cost(70), 23.10);
        assert_eq!(get_sell_cost(60), 30.0);
        assert_eq!(get_recycle(60, 70), 0.50);
        assert_eq!(get_day_profit(70, 0.5, 0.3), 7.40);
    }
    #[test]
    fn day_profit2() {
        assert_eq!(get_day_profit(70, 0.5, 0.15), 2.90);
    }

    #[test]
    fn day_demand_good() {
        assert_eq!(get_day_demand(0.33, &GOOD_PROBABILITIES, GOOD_DEMANDS), 70)
    }
    #[test]
    fn day_demand_fair() {
        assert_eq!(get_day_demand(0.33, &FAIR_PROBABILITIES, FAIR_DEMANDS), 60)
    }
    #[test]
    fn day_demand_poor() {
        assert_eq!(get_day_demand(0.33, &POOR_PROBABILITIES, POOR_DEMANDS), 40)
    }
}
